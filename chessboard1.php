<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Dynamic Chessboard Using PHP</title>
	<link rel="stylesheet" href="style.css" />
</head>
<body>
	<form action="" method="post">
		<label for="number">Input Number:
		<input type="text" name="number" id="number">
		<input type="submit" value="Draw Chessboard">
		</label>
	</form>
<div class = "row">
	<?php
		if(isset($_POST["number"])){
		$number = $_POST["number"];
		  for($row=$number;$row>0;$row--)
		  {
			  for($col=0;$col<$number;$col++)
			  {
			  $total=$row+$col;
			  if($total%2==0)
			  {
				  $color = 'white';
			  }
			  else
			  {
				   $color = 'black';
			  }?>
				<div class="<?php echo $color;?>"></div>
			  <?php
			  }
			echo "<br>";
		}
		}
	?>
	</div>

</body>
</html>